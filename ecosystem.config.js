module.exports = {
    apps : [
        {
            name: "all in one",
            script: "./index.js",
            watch: true,
            env: {
                "NODE_ENV": "development",
            },
            env_production: {
                "PORT": 3000,
                "NODE_ENV": "production"
            }
        }
    ]
}  