const express = require("express");
const fs = require("fs");
const uuidv4 = require("uuid/v4");
const bodyParser = require("body-parser");
const next = require("next");
const cors = require("cors");

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare()
.then(() => {
    const server = express();

    server.use(bodyParser.urlencoded({ 
        extended: false, 
        limit: "5mb" 
    }));
    
    server.use(bodyParser.json({ 
        limit: "5mb" 
    }));

    server.use(cors());

    // Salva um base64 em forma de arquivo e envia a url
    server.post("/api/urlfile", (req, res) => {
        const { base64, formato } = req.body;

        const fileName = `./static/temp/${uuidv4()}${formato}`;

        try {
            let buf = new Buffer(base64, 'base64');
            fs.writeFileSync(fileName, buf);

            res.json({ sucesso: true, pathFile: fileName.replace(".","") });

            setTimeout(() => {
                fs.unlinkSync(fileName);
            }, 60000);
        }catch(err) {
            console.error(err);
            res.json({ sucesso: false, error: err });
        }
    });

    server.get("*", (req, res) => handle(req, res));

    const port = 3000;

    server.listen(port, (err) => {
        if(err) throw err;
        console.log(`> Ready on http://localhost:${port}`);
    });
})
.catch((ex) => {
    console.error(ex.stack);
    process.exit(1);
});

module.exports = app;