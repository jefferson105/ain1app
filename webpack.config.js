const path = require("path");
const webpack = require("webpack");
//const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");
const NodemonPlugin = require("nodemon-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
require('regenerator-runtime/runtime');

module.exports = {
    entry: {
        js: ["./src/js/index.js"]
    },
    output: {
        path: path.join(__dirname, "/public/js"),
        filename: "bundle.js"
    },
    devtool: "#source-map",
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                loader: "babel-loader",
                query: {
                    presets: ["env", "stage-0", "react"],
                    plugins: ["transform-async-to-generator", "transform-runtime"]
                }
            },
            {
                test: /\.scss$/,
                loaders: [ 'style-loader', 'css-loader?sourceMap', 'sass-loader?sourceMap' ]  
            }
        ]
    },
    plugins: [
        new NodemonPlugin({
            watch: path.resolve("./index.js"),
            ignore: ["*.js.map"],
            verbose: true,
            script: "./index.js"
        }),
        /*new UglifyJSPlugin({
            sourceMap: true,
            uglifyOptions: { ecma: 8 }
        }),*/
        new BrowserSyncPlugin({
            host: "localhost",
            port: 3000,
            proxy: "http://localhost:9090/"
        })
    ]
};