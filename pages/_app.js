import App, { Container } from "next/app";
import { connect } from "react-redux";
import { Provider } from "react-redux";
import withRedux from "next-redux-wrapper";

import createStore from "../lib/store";

import BaseFrame from "../components/BaseFrame";
import Loading from "../components/Loading";
import Erro from "../components/Error";

import '../scss/main.scss';

class MyApp extends App {
    constructor(props) {
        super(props);

        this.state = {
            loadingSKD: true,
        }
    }

    componentDidMount() {
        window.fbAsyncInit = () => {
            FB.init({
                appId            : '167446703852071',
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v3.0'
            });

            this.setState({ loadingSKD: false });
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/pt_BR/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }

    render() {
        const { loadingSKD } = this.state;
        const { Component, pageProps, store, loading, erro } = this.props;

        return (
            <Container>
                <Provider store={store}>
                    {
                        loadingSKD ?
                            <p>Carregando informações</p> :
                            <BaseFrame>
                                <Component {...pageProps} /> 
                                { loading && <Loading /> }
                                { !!erro && <Erro /> }
                            </BaseFrame>                                
                    }
                </Provider>
            </Container>
        );
    }
}

export default withRedux(createStore)(connect(state => state)(MyApp));