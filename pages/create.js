import { connect } from "react-redux";

import { sendPost } from "../lib/actions";

class Create extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isInsta: false,
            file: {},
            mensagem: ""
        }

        this.loadPhoto = this.loadPhoto.bind(this);
        this.bindInput = this.bindInput.bind(this);
        this.sendPost = this.sendPost.bind(this);
    }

    bindInput(e) {
        e.preventDefault();
        this.inputFoto.click();
    }

    loadPhoto(e) {
        let _this = e.target;
        var reader = new FileReader();
        
        reader.onload = (event) => {
            var infoFile = _this.files[0];

            if(infoFile.size > 15000000)
                return;

            if(infoFile.type.indexOf("image") > -1) {
                this.setState({ file: { base64: event.target.result, type: "image" }});
            }else if(infoFile.type.indexOf("video") > -1) {
                this.setState({ file: { base64: event.target.result, type: "video" }});
            }
        };

        reader.readAsDataURL(_this.files[0]);   
    }

    async sendPost(e) {
        e.preventDefault();

        if(!!this.state.mensagem) {
            if(!!this.state.file.base64) {
                try {
                    this.setState({ loading: true });

                    let splitData = this.state.file.base64.split(";base64,");
    
                    var res = await fetch(`http://allinonedigitalmarketing.com.br/api/urlfile`, {
                        method: "POST", 
                        headers: {
                            "Content-Type": "application/json"
                        }, 
                        body: JSON.stringify({
                            formato: `.${splitData[0].split("/")[1]}`,
                            base64: splitData[1]
                        })
                    });
    
                    var { sucesso, error, pathFile } = await res.json();
    
                    if(sucesso) {
                        var { mensagem, isInsta } = this.state;
                        var picture = this.state.file.base64;
                        
                        if(isInsta && !picture)
                            throw new Error("Instagram deve ter imagem");

                        setTimeout(() => {
                            this.props.dispatch(
                                sendPost(mensagem, "http://allinonedigitalmarketing.com.br" + pathFile, picture, isInsta)
                            );
                        }, 1000)
                    }else {
                        this.props.dispatch({ type: "SET_ERROR", data: JSON.stringify(error)});
                    }
                }catch(err) {
                    this.props.dispatch({ type: "SET_ERROR", data: JSON.stringify(err)});
                }
            }else {
                this.props.dispatch(sendPost(this.state.mensagem))
            }
        }else {
            this.props.dispatch({ type: "SET_ERROR", data: "Mensagem obrigatória"});
        }
        this.setState({ file: {}, mensagem: "" });
        this.textarea.value = "";
    }
    
    render() {
        const { mensagem, file, isInsta } = this.state;

        return (
            <React.Fragment>
                <form className="formPost">
                    <label className="formPost__label" htmlFor="mensagem-post">Mensagem</label>
                    <textarea 
                        className="formPost__msg" 
                        defaultValue={mensagem} 
                        onChange={(e) => this.setState({ mensagem: e.target.value })} 
                        ref={(_this) => this.textarea = _this}
                        id="mensagem-post"
                    >
                    </textarea>
                    
                    <button onClick={this.bindInput} className="formPost__photo">Carregue uma foto ou um vídeo</button>
                    <input ref={(input) => this.inputFoto = input} onChange={this.loadPhoto} className="formPost__inputFile" type="file" accept="image/png,image/jpg,image/jpeg,image/gif,image/bmp,image/x-bmp,image/tiff" id="file-post" />
                    
                    {
                        Object.keys(file).length ? 
                            file.type == "image" ?
                                <figure className="formPost__loadedImg">
                                    <img className="formPost__img" id="img-post" src={file.base64} />
                                </figure> :
                                <video controls src={file.base64} /> : null
                    }
                    
                    { <button onClick={this.sendPost} className="formPost__send">Send Post <img src="/static/img/send-button.svg" /></button> }
                
                    { !!this.props.instagram && <label htmlFor="isInsta" className="formPost__insta">
                        <input id="isInsta" checked={isInsta} onChange={() => this.setState({ isInsta: !isInsta })} type="checkbox" />
                        <span>Enviar para Instagram</span>
                    </label> }
                </form>        
            </React.Fragment>
        );
    }
}

export default connect(state => state)(Create)