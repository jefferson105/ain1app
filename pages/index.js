import { connect } from "react-redux";

import Notifications from "../components/Notifications";
import Inbox from "../components/Inbox";
import Posts from "../components/Posts";
import Conversa from "../components/Conversa";

const Index = ({ inbox, instagram }) =>
    <React.Fragment>
        <div className="main-content__divContent">
            <div className="verticalList">
                <h2>Notificações</h2>
                <Notifications />                       
            </div>
            <div className="inbox verticalList">
                <h2>Inbox</h2>
                <Inbox />                      
            </div>
        </div>
        <div className="main-content__posts">
            <h2 className="main__title">Posts</h2>
            <Posts />
        </div>
        { 
            instagram && 
            <div className="main-content__posts">
                <h2 className="main__title">Instagram</h2>
                <Posts type="instagram" />
            </div>
        }
        
        { inbox.infoInbox && <Conversa /> }
    </React.Fragment>

export default connect(state => state)(Index);