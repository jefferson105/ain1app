import { connect } from "react-redux";

import Comments from "./Comments";
import { ColumnChart } from 'react-chartkick';

import { addCommentNext, getInsightsInsta, getComment } from "../lib/actions";

class Instagram extends React.Component {
    constructor(props) {
        super(props);

        this.listenInput = this.listenInput.bind(this);
    }

    listenInput(e) {
        if(e.key == "Enter") {
            this.props.dispatch(addCommentNext(null, this.props.id, e.target.value, false, true));
            e.target.value = "";
        }
    }

    render() {
        const { caption, showInsights, showComments, insights, id, media_type, comments, media_url, timestamp, dispatch } = this.props;
        return(
            <li className="post-list__item">
                <small>{timestamp.slice(0, 10)}</small>
                { media_type == "IMAGE" && <img className="post-list__pic" src={media_url} /> }
                { media_type == "VIDEO" && <video className="post-list__pic" src={media_url}></video> }
                { !!caption && <p>{caption}</p> }
                <input className="post-list__input" onKeyUp={this.listenInput} type="text" placeholder="Comentar" />
                <div className="post-list__controll">
                    <button 
                        onClick={() => dispatch(getComment(id, true))}
                        className="post-list__button" 
                        title="Ver comentários"
                    >
                        <img className="post-list__icon" src="/static/img/chat.svg" /> {!!comments && Object.keys(comments).length}
                    </button>
                    <button onClick={() => dispatch(getInsightsInsta(id))} className="post-list__button" title="Ver insights">
                        <img 
                            className="post-list__icon" 
                            src="/static/img/bar-chart.svg" 
                        />
                    </button>
                </div>
                { showComments && <Comments comments={comments} postId={id} /> }
                { showInsights && <ColumnChart data={insights} /> }
            </li>
        )
    }
} 

export default connect(state => state)(Instagram);