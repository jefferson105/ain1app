import { connect } from "react-redux";

import Login from "./signIn";
import Header from "./header";
import SideMenu from "./sideMenu";

import { showLogin, getUserInfo } from "../lib/actions";

class BaseFrame extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        FB.getLoginStatus(({ status }) => {
            if(status === "connected") {
                this.props.dispatch(getUserInfo(true));
            }else {
                this.props.dispatch(showLogin());
            }
        });
    }

    render() {
        const { loadingInit, showLogin, actualPage } = this.props;

        return (
            <React.Fragment>
                { 
                    (loadingInit) ?
                        <p>Carregando</p> :
                        showLogin ?
                        <Login /> :
                        !!actualPage ?
                        <React.Fragment>
                            <Header />
                            <main>
                                <SideMenu />
                                <section className="main-content">
                                    {this.props.children}
                                </section>
                            </main>
                        </React.Fragment> :
                        <p>Você não tem páginas para administrar</p> 
                }
            </React.Fragment>
        );
    }
}

export default connect(state => state)(BaseFrame);