import { connect } from "react-redux";

import { getComments } from "../lib/api";
import Comment from "./Comment";

class Comments extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { comments, postId } = this.props;

        return (
            <React.Fragment>
                <ul className="comments-post">
                    {
                        (Object.values(comments).length) ?
                            Object.values(comments).map((c, i) => 
                                <Comment key={i} postId={postId} getEnter={this.listenInput} {...c} />
                            ) : 
                            <li className="comments-post__item">Esse post ainda não tem comentário</li>
                    }
                </ul>
            </React.Fragment>
        )
    }
}   

export default connect(state => state)(Comments);