import { connect } from "react-redux";

import { openInbox } from "../lib/actions";

const Inbox = ({ inbox, dispatch }) =>
    <ul className="main__list verticalList__list">
        {   
            !!inbox.error ?
            <li>{inbox.error}</li> :
            inbox.loading ?
            <li><img className="spinner" src="/static/img/spinner-of-dots_black.svg" /></li> :
            (Object.keys(inbox.list).length) ?
                Object.values(inbox.list).map((ib, i) => 
                    <li 
                        key={i} 
                        onClick={() => dispatch(openInbox(ib))}
                        className="inbox__item commom-list__item">
                            <figure>
                                <img src={ib.picture} />
                            </figure>
                            <div>
                                <h4>{ib.participants.data[0].name}</h4>
                                <p>{ib.snippet}</p>
                            </div>
                    </li>
                ) :
                <li>Você não tem menssagens na caixa de entrada</li>
        }
    </ul>

export default connect(state => state)(Inbox);