import Link from 'next/link';

export default () => 
    <aside className="aside">
        <nav className="aside__menu">
            <ul className="aside__list">
                <li className="aside__item">
                    <Link prefetch href="/">
                        <a className="aside__link">Início</a>
                    </Link>
                </li>
                <li className="aside__item">
                    <Link prefetch href="/create">
                        <a className="aside__link">Novo post</a>
                    </Link>
                </li>
                <li className="aside__item">
                    <Link prefetch href="#">
                        <a className="aside__link">Sendy</a>
                    </Link>
                </li>
            </ul>
        </nav>
    </aside>

/*
    <li className="aside-menu-list-item">
        <Link prefetch href="#">
            <a>Publicações</a>
        </Link>
    </li>
*/