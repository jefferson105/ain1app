import { connect } from "react-redux";

import { setPage } from "../lib/actions";

class Header extends React.Component {
    constructor(props) {
        super(props);
    
        this.changePage = this.changePage.bind(this);
    }
    
    changePage(event) {
        let _this = event.target.nodeName == "LI" ? event.target : event.target.parentNode;
        let numPage = _this.dataset.num;
        this.props.dispatch(setPage(numPage));
    }

    render() {
        const { name, picture, accounts } = this.props.user;
        const { actualPage } = this.props;

        return(
            <header className="header">
                <ul className="header__info">
                    <li className="header__userdata">
                        <img src={picture.data.url} /> 
                        <figcaption>{name}</figcaption>                       
                    </li>
                    <li className="header__userpages">
                        <ul className="header__list">
                        {
                            accounts.data.map(({ name, id, picture }, i) => 
                                <React.Fragment key={i}>
                                    {
                                    (id == actualPage.id) ?
                                        <li className="selected header__item">
                                            <img src={picture.data.url} />
                                            {name}
                                        </li> : 
                                        <li data-num={i} onClick={this.changePage} className="header__item">
                                            <img src={picture.data.url} />
                                            {name}
                                        </li>
                                    }
                                </React.Fragment>
                            )
                        }
                        </ul>
                    </li>
                </ul>
            </header>
        )
    }
}

export default connect(state => state)(Header);