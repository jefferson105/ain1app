import { connect } from "react-redux";

import { setNotifications } from "../lib/actions"; 

class Notifications extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { notifications } = this.props;

        return (
            <ul className="main__list verticalList__list">
                {   
                    !!notifications.error ?
                    <li>{notifications.error}</li> :
                    notifications.loading ?
                        <li><img className="spinner" src="/static/img/spinner-of-dots_black.svg" /></li> : 
                        (notifications.list.length) ?
                            notifications.list.map(({ title, from, link }, i) =>
                                <a className="commom-list__item" target="_blank" key={i} href={link}>
                                    <img src={from ? from.picture.data.url : "https://static.xx.fbcdn.net/rsrc.php/v3/yo/r/UlIqmHJn-SK.gif"} /> 
                                    {title}
                                </a>
                            ) :
                            <li>Você não tem notificações</li>
                }
            </ul>
        )
    }
}

export default connect(state => state)(Notifications);