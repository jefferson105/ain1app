import { connect } from "react-redux";

import Comments from "./Comments";
import { ColumnChart } from 'react-chartkick';

import { addCommentNext, likePost, deletePosts, sendPost, getComment, getInsights } from "../lib/actions";

class Post extends React.Component {
    constructor(props) {
        super(props);

        this.listenInput = this.listenInput.bind(this);
    }

    listenInput(e) {
        if(e.key == "Enter") {
            this.props.dispatch(addCommentNext(null, this.props.id, e.target.value, false));
            e.target.value = "";
        }
    }

    render() {
        const { created_time, id, message, name, picture, comments, insights, likes = {summary: {}}, showComments, showInsights, dispatch, index } = this.props;

        return(
            <li className={`post-list__item ${!!(index > 3) && "hide"}`}>
                <small>{!!created_time && created_time.slice(0, 10)}</small>
                { !!name && <h3>{name}</h3> }
                { !!picture && <img className="post-list__pic" src={picture} /> }
                { !!message && <p className="post-list__msg">{ message }</p> }
                <input className="post-list__input" onKeyUp={this.listenInput} type="text" placeholder="Comentar" />
                <div className="post-list__controll">
                    <button onClick={() => dispatch(likePost(id))} className="post-list__button" title="Like">
                        <img className="post-list__icon" src={`/static/img/thumb-up${likes.summary.has_liked ? `.active` : ""}.svg`} /> {likes.summary.total_count}
                    </button>
                    <button 
                        onClick={() => dispatch(getComment(id))}
                        className="post-list__button" 
                        title="Ver comentários"
                    >
                        <img className="post-list__icon" src="/static/img/chat.svg" /> {!!comments && Object.keys(comments).length}
                    </button>
                    <button className="post-list__button" title="Ver insights">
                        <img onClick={() => this.setState({ showInsight: dispatch(getInsights(id)) })} className="post-list__icon" src="/static/img/bar-chart.svg" />
                    </button>
                </div>
                { showComments && <Comments comments={comments} postId={id} /> }
                { showInsights && <ColumnChart data={insights} /> }
                <figure className="post-list__actions">
                    { !!index && <img onClick={() => dispatch(sendPost(message, picture, picture))} title="Repostar" className="post-list__act post-list__act--marged" src="/static/img/circular-arrow.svg" /> }
                    <img onClick={() => dispatch(deletePosts(id))} title="Deletar Post" className="post-list__act" src="/static/img/delete.svg" />
                </figure>
            </li>
        )
    }
}

export default connect(state => state)(Post);