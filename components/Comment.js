import { connect } from "react-redux";

import { getCommentsNest, addCommentNext } from "../lib/actions";

class Comment extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showComments: false
        }

        this.listenInput = this.listenInput.bind(this);
        this.getComments = this.getComments.bind(this);
    }

    listenInput(e) {
        if(e.key == "Enter") {
            this.props.dispatch(addCommentNext(this.props.id, this.props.postId, e.target.value, true));
            e.target.value = "";
        }
    }

    getComments() {
        const { dispatch, id, postId, fetched } = this.props;

        this.setState({ showComments: true });

        if(fetched)
            return;

        dispatch(getCommentsNest(postId, id));
    }
    
    render() {
        const { created_time, from, id, message, getEnter, comments, fetched } = this.props;
        const { showComments } = this.state;
        return(
            <li className="comments-post__item">
                {
                    !!(from != undefined) &&
                    <React.Fragment>
                        <figure className="comments-post__photo">
                            <img src={from.picture.data.url} />
                        </figure>
                        <div className="comments-post__info">
                            <p><b>{from.name}</b></p>
                            <p>{message}</p>
                            <small>{created_time.slice(0,10)}</small><br />
                            <input onKeyUp={this.listenInput} placeholder="Responder" type="text" />
                            {
                                showComments ? 
                                    <span onClick={() => this.setState({ showComments: false })} className="comments-post__ver">Esconder</span> : 
                                    <span onClick={this.getComments} className="comments-post__ver">Mostrar comentários</span> 
                            }
                            <div className={showComments ? "" : "hide"}>
                            {
                                comments && comments.length ?
                                    comments.map((c, i) => 
                                        <SubComment key={i} {...c} />
                                    ) : <span>Nenhum comentário</span>
                            }
                            </div>
                        </div>
                    </React.Fragment> 
                }
            </li>
        );
    }
}

const SubComment = ({ from, message, created_time }) => 
    <div className="comments-post__item">
        <figure className="comments-post__photo">
            <img src={from.picture.data.url} />
        </figure>
        <div className="comments-post__info">
            <p><b>{from.name}</b></p>
            <p>{message}</p>
            <small>{created_time.slice(0,10)}</small>
        </div>
    </div>

export default connect(state => state)(Comment);