import { connect } from "react-redux";

import Post from "./Post";
import Instagram from "./Instagram";

class Posts extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            slide: 0
        }

        this.slider = this.slider.bind(this);
    }

    slider(dir) {
        let { slide } = this.state;
        if(dir == "left") {
            if(slide > 0)
                slide--;
        }else {
            var showElem = Array.from(this.list.children).filter((el) => Array.from(el.classList).indexOf("hide") == -1);

            var lastElem = showElem[showElem.length - 1]

            if(lastElem.nextSibling) {
                lastElem = lastElem.nextSibling;
                lastElem.classList.remove("hide");
            }

            if(this.list.clientWidth < lastElem.getBoundingClientRect().left)
                slide++;
        }

        this.setState({ slide });
    }

    componentWillUpdate(next) {
        if(this.props.type == "instagram") {
            if(this.props.instagram.list != next.instagram.list){
                this.setState({ slide: 0 });
            }
        }else {
            if(this.props.posts.list != next.posts.list) {
                this.setState({ slide: 0 });
            }
        }
    }

    render() {
        const { posts, instagram, type } = this.props;
        const { slide } = this.state;

        var allPosts = type == "instagram" ? instagram : posts;

        return (
            <div className="post-list">
                <button onClick={this.slider.bind(this, "left")} className="post-list__arrow">
                    <img src="/static/img/slide-left.svg" />
                </button>
                <nav className="post-list__nav">
                    <ul ref={(el) => this.list = el} style={{ transform: `translateX(-${slide * 25}%)` }} onMouseDown={this.sliderItem} id="listPost" className="listPost post-list__list main-content__list">
                        {
                            !!posts.error ?
                                <li>{posts.error}</li> : 
                                posts.loading ?
                                    <li><img className="spinner" src="/static/img/spinner-of-dots_black.svg" /></li> :
                                    (Object.values(allPosts.list).length) ?
                                        Object.values(allPosts.list).map((post, i) => type == "instagram" ? 
                                            <Instagram key={i} index={i} {...post} /> : 
                                            <Post key={i} index={i} {...post} />) :
                                        <li>Você não tem posts</li>
                        }
                    </ul>
                </nav>
                <button onClick={this.slider.bind(this, "right")} className="post-list__arrow">
                    <img src="/static/img/slide-right.svg" />
                </button>
            </div>
        )
    }
}

export default connect(state => state)(Posts);