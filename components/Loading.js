export default () => 
    <div className="loading">
        <img className="loading__img" src="/static/img/spinner-of-dots.svg" />
    </div>