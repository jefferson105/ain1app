import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import { connect } from "react-redux";

import { closeInbox, sendInbox } from "../lib/actions";

const Conversa = ({ inbox, dispatch }) => 
    <ReactCSSTransitionGroup
        transitionName="box"
        transitionAppear={true}
        transitionAppearTimeout={50}
        transitionEnter={true}
        transitionEnterTimeout={50}
        transitionLeave={true}
        transitionLeaveTimeout={50}
    >
        <div className="box-conversa">
            <div className="box-conversa__container">
                <img onClick={() => dispatch(closeInbox())} src="/static/img/close.svg" className="box-conversa__close" />
                <header className="box-conversa__header">
                    <img className="box-conversa__foto-perfil" src={inbox.infoInbox.picture} />
                    <h4>{inbox.infoInbox.participants.data[0].name}</h4>
                </header>
                <div className="box-conversa__mensagens">
                    {
                        inbox.infoInbox.messages.data.length ?
                            inbox.infoInbox.messages.data.map((m, i) => 
                                <p 
                                    className={`box-conversa__${m.from.name == inbox.infoInbox.participants.data[0].name ? "outro" : "me"}`} 
                                    key={i}>
                                    {m.message}
                                </p>) : 
                            <p>Envie uma mensagem</p>
                    }
                </div>
                <footer className="box-conversa__footer">
                    <input ref={(input) => this.input = input} onKeyUp={inputSend.bind(null, dispatch, inbox.infoInbox.id )} className="box-conversa__input" placeholder="Envie uma mensagem" type="text" />
                    <img onClick={() => inputSend(dispatch, this.input.value, inbox.infoInbox.id)} className="box-conversa__send" src="/static/img/send-button.svg" />
                </footer>
            </div>
        </div>
    </ReactCSSTransitionGroup>

const inputSend = (dispatch, id, { key, target }) => {
    let value = target.value;
    if(key == "Enter" && value) {
        dispatch(sendInbox(id, value));
        target.value = "";
    }
}

export default connect(state => state)(Conversa);

/*
    transitionName="box"
    transitionAppear={true}
    transitionAppearTimeout={500}
    transitionEnter={false}
    transitionLeave={false}
*/