import { connect } from "react-redux";

import { showLogin, getUserInfo } from "../lib/actions";

class SignIn extends React.Component {
    constructor(props) {
        super(props);
        this.loginFacebook = this.loginFacebook.bind(this);
    }

    loginFacebook() {
        FB.login(({ authResponse, status }) => {
            if(status === "connected") {
                this.props.dispatch(getUserInfo(true));
            }else {
                this.props.dispatch(showLogin());
            }
        }, {
            scope: "email,public_profile,user_friends,manage_pages,publish_pages,read_insights,read_page_mailboxes,instagram_basic,instagram_manage_comments,instagram_manage_insights",
            return_scopes: true
        });
    }

    render() {
        return (
            <div className="container-login">
                <div className="login">
                    <img className="login-logo" src="/static/img/logo-app.png" />
                    <h2 className="login-title">Acesso</h2>
                    <p className="login-txt">Bem vindo ao All in One.</p>
                    <p className="login-txt">Para acessar é necessário que você se identifique através do Facebook</p>
                    
                    <p onClick={this.loginFacebook}>Entar com Facebook</p>
                </div>
            </div>
        );
    }
}

export default connect(state => state)(SignIn);

//<fb:login-button scope="email,public_profile,user_friends,manage_pages,publish_pages,read_insights,read_page_mailboxes,instagram_basic,instagram_manage_comments,instagram_manage_insights" onlogin={this.loginFacebook}></fb:login-button>
//<div class="fb-login-button login-facebook" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false" scope="email,public_profile,user_friends,manage_pages,publish_pages,read_insights,read_page_mailboxes,instagram_basic,instagram_manage_comments,instagram_manage_insights" onlogin={this.loginFacebook}></div>
//<FacebookProvider appId="167446703852071"><Login scope="email,public_profile,user_friends,manage_pages,publish_pages,read_insights,read_page_mailboxes,instagram_basic,instagram_manage_comments,instagram_manage_insights" onResponse={this.loginFacebook} onError={this.handleError}><span>Login via Facebook</span></Login></FacebookProvider>