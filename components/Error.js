import { connect } from "react-redux";

import { closeError } from "../lib/actions";

const Erro = ({ erro, dispatch }) => 
    <div className="erro" onClick={closeErro.bind(this, dispatch)}>
        <div onClick={(e) => e.stopPropagation()} className="erro__box">
            <p className="erro__close">Erro: <span onClick={closeErro.bind(this, dispatch)} className="erro__x">x</span></p>
            <p className="erro__msg">{erro}</p>
        </div>
    </div>

const closeErro = (dispatch, e) => {
    e.stopPropagation();
    dispatch(closeError());
}

export default connect(state => state)(Erro);