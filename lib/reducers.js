import { actionTypes } from "./actions";

export const initialData = {
    user: null,
    loading: true,
    loadingInit: true,
    showLogin: true,
    actualPage: null,
    erro: null,
    notifications: {
       list: [],
       loading: true,
       error: null
    },
    inbox: {
        list: {},
        loading: true,
        error: null,
        infoInbox: null
    },
    posts: {
        list: [],
        loading: true,
        error: null
    },
    instagram: null
};

const reducer = (state = initialData, { type, data }) => {
    switch(type) {
        case actionTypes.SET_ERROR: 
            return {
                ...state,
                erro: data
            }

        case actionTypes.LOADING_ON:
            return {
                ...state,
                loading: true
            }

        case actionTypes.LOADING_OFF:
            return {
                ...state,
                loading: false
            }
        
        case actionTypes.SWITCH_LOADING_INIT: 
            return {
                ...state,
                loadingInit: !state.loadingInit
            }

        case actionTypes.OFF_LOADING_INIT:
            return {
                ...state,
                loadingInit: false
            }

        case actionTypes.SWITCH_LOADING:
            return {
                ...state,
                loading: !state.loading
            }

        case actionTypes.SHOW_LOGIN:
            return {
                ...state,
                showLogin: true
            }
        
        case actionTypes.HIDE_LOGIN:
            return {
                ...state,
                showLogin: false
            }

        case actionTypes.GET_USER_INFO:
            return {
                ...state,
                user: data
            };

        case actionTypes.SET_PAGE:
            return {
                ...state,
                actualPage: data
            };

        case actionTypes.GET_NOTIFICATIONS:
            return {
                ...state,
                notifications: data
            }

        case actionTypes.GET_INBOX:
            return {
                ...state,
                inbox: data
            }

        case actionTypes.GET_POSTS: case actionTypes.SET_POSTS:
            return {
                ...state,
                posts: data
            }

        case actionTypes.SET_COMMENT:
            return {
                ...state
            }

        case actionTypes.OPEN_INBOX:
            return {
                ...state,
                inbox: {
                    ...state.inbox,
                    infoInbox: data
                }
            }

        case actionTypes.CLOSE_INBOX:
            return {
                ...state,
                inbox: {
                    ...state.inbox,
                    infoInbox: null
                }
            }

        case actionTypes.GET_INSTA:
            return {
                ...state,
                instagram: data
            }

        default:
            return state;
    }
}

export default reducer;