export const loginStatus = () =>
    new Promise((resolve, reject) => {
        FB.getLoginStatus(({ status, authResponse }) => {
            if(status === "connected") {
                resolve({ connected: true })
            }else {
                resolve({ connected: false });
            }
        });
    });

export const getUserInfo = () => 
    new Promise((resolve, reject) => {
        FB.api(
            '/me',
            'GET',
            {"fields":"id,name,accounts{name,id,picture,access_token},picture"},
            (data) => {
                if(data.error) 
                    reject(data);

                resolve(data);
            }
        );
    });

export const userInformation = (id) => {
    return new Promise((resolve, reject) => {
        FB.api(
            '/'+id,
            'GET',
            {"fields":"name,picture"},
            (data) => {
                if(data.error) 
                    reject(data);

                resolve(data);
            }
        );
    });
};

export const getUserPic = (id) => {
    return new Promise((resolve, reject) => {
        FB.api(
            `/${id}/picture`,
            'GET',
            {redirect: false},
            (data) => {
                if(data.error) 
                    reject(data);

                resolve(data);
            }
        );
    })
}

export const getNotifications = (pageId, token) =>
    new Promise((resolve, reject) => {
        FB.api(
            `/${pageId}/notifications`,
            'GET',
            {"fields":"title,from{name,picture},link", "access_token": token},
            (data) => {
                if(data.error) 
                    reject(data);

                resolve(data);
            }
        );
    });

export const getInbox = (pageId, token) => 
    new Promise((resolve, reject) => {
        FB.api(
            `/${pageId}/conversations/`,
            'GET',
            {"fields":"snippet,id,participants,updated_time,messages{message,from,created_time}", "access_token": token},
            (data) => {
                if(data.error) 
                    reject(data);

                resolve(data);
            }
        );
    });

export const getPosts = (pageId, token) => 
    new Promise((resolve, reject) => {
        FB.api(
            `/${pageId}/feed`,
            'GET',
            {"fields":"description,picture,name,likes.summary(true),message,created_time", "access_token": token},
            (data) => {
                if(data.error) 
                    reject(data);

                resolve(data);
            }
        );
    });

export const likePost = (postId, token) =>
    new Promise((resolve, reject) => {
        FB.api(
            `/${postId}/likes`,
            "POST",
            {"access_token": token},
            (data) => {
                if(data.error) 
                    reject(data);

                resolve(data);
            }
        );
    });

export const removeLikePost = (postId, token) =>
    new Promise((resolve, reject) => {
        FB.api(
            `/${postId}/likes`,
            "DELETE",
            {"access_token": token},
            (data) => {
                if(data.error) 
                    reject(data);

                resolve(data);
            }
        );
    });

export const getComments = (id, token) => 
    new Promise((resolve, reject) => {
        FB.api(
            `/${id}/comments/`,
            'GET',
            {"fields":"from{id,name,picture},created_time,message,id", "access_token": token},
            (data) => {
                if(data.error) 
                    reject(data);

                resolve(data);
            }
        );
    });

export const getInsights = (id, token) => 
    new Promise((resolve, reject) => {
        FB.api(
            `/${id}/insights`,
            'GET',
            {"fields":"values,name,title,description,period","metric":"post_impressions_organic_unique,post_impressions_paid_unique,post_negative_feedback_unique,post_engaged_users","period":"lifetime","access_token":token},
            (data) => {
                if(data.error) 
                    reject(data);

                resolve(data);
            }
        );
    });

export const getInsightsInsta = (id, token) => 
    new Promise((resolve, reject) => {
        FB.api(
            `/${id}/insights`,
            'GET',
            {"metric":"impressions,reach","period":"lifetime","fieldsvalues,name,title,description,period":null},
            (data) => {
                if(data.error) 
                    reject(data);

                resolve(data);
            }
        );
    });

export const sendComment = (id, token, msg) => 
    new Promise((resolve, reject) => {
        FB.api(
            `/${id}/comments/`,
            'POST',
            {"message": msg, "access_token": token},
            (data) => {
                if(data.error) 
                    reject(data);

                resolve(data);
            }
        );
    });

export const sendCommentInsta = (id, token, msg) => 
    new Promise((resolve, reject) => {
        FB.api(
            `/media/${id}/comments/`,
            'POST',
            {"message": msg, "access_token": token},
            (data) => {
                if(data.error) 
                    reject(data);

                resolve(data);
            }
        );
    });

export const sendInbox = (idConversa, token, msg) => {
    return new Promise((resolve, reject) => {
        FB.api(
            `/${idConversa}/messages`,
            'POST',
            {"access_token":token,"message": msg},
            (data) => {
                if(data.error) 
                    reject(data);
    
                resolve(data);
            }
        );
    });
};

export const deletePost = (id, token) => {
    return new Promise((resolve, reject) => {
        FB.api(
            `/${id}`,
            'DELETE',
            { "access_token": token },
            (data) => {
                if(data.error) 
                    reject(data);
    
                resolve(data);
            }
        );
    });
}

export const sendPost = (id, token, message, isInsta) => {
    return new Promise((resolve, reject) => {
        FB.api(
            `/${id}/${isInsta?"media":"feed"}`,
            'POST',
            {"access_token": token, message},
            (data) => {
                if(data.error) 
                    reject(data);
    
                resolve(data);
            }
        );
    });
};

export const sendMedia = (id, access_token, caption, url, isInsta) => {
    return new Promise((resolve, reject) => {
        FB.api(
            `/${id}/${isInsta?"media":"photos"}`,
            'POST',
            { access_token, caption, url },
            (data) => {
                if(data.error) 
                    reject(data);
    
                resolve(data);
            }
        );
    });
};

export const pageInsta = (idPage) => {
    return new Promise((resolve, reject) => {
        FB.api(
            `/${idPage}`,
            'GET',
            {"fields":"instagram_business_account"},
            async (data) => {
                if(data.error) {
                    reject(data);
                    return;
                }

                if(data.instagram_business_account != undefined) {
                    let res = await getPostsInsta(data.instagram_business_account.id);

                    resolve(res);
                }else {
                    resolve(null);
                }
            }
        );
    });
}

export const getPostsInsta = (idInsta) => {
    return new Promise((resolve, reject) => {
        FB.api(
            `/${idInsta}`,
            'GET',
            {"fields":"media{id,media_type,media,caption,comments{text,user,media,replies{text,user,timestamp},id,like_count,timestamp},comments_count,media_url,timestamp}"},
            (data) => {
                if(data.error) 
                    reject(data);
    
                resolve(data);
            }
        );
    })
}

//scheduled_publish_time