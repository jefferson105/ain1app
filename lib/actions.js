import * as api from "../lib/api";
import Moment from "moment";

export const actionTypes = {
    SWITCH_LOADING: "SWITCH_LADING",
    LOADING_ON: "LOADING_ON",
    LOADING_OFF: "LOADING_OFF",
    SWITCH_LOADING_INIT: "LOADING_INIT",
    OFF_LOADING_INIT: "OFF_LOADING_INIT",
    GET_USER_INFO: "GET_USER_INFO",
    SET_USER_INFO: "SET_USER_INFO",
    SET_PAGE: "SET_PAGE",
    GET_POSTS: "GET_POSTS",
    GET_NOTIFICATIONS: "GET_NOTIFICATIONS",
    GET_INBOX: "GET_INBOX",
    OPEN_INBOX: "OPEN_INBOX",
    CLOSE_INBOX: "CLOSE_INBOX",
    SET_POSTS: "SET_POSTS",
    GET_INSTA: "GET_INSTA",
    SHOW_LOGIN: "SHOW_LOGIN",
    HIDE_LOGIN: "HIDE_LOGIN",
    SET_ERROR: "SET_ERROR"
}

// Switch Load Page ON/OFF
export function switchLoad() {
    return { type: actionTypes.SWITCH_LOADING };
}

// Switch Loading initial
export function switchLoadInit() {
    return { type: actionTypes.SWITCH_LOADING_INIT };
}

export function loading_on() {
    return { type: actionTypes.LOADING_ON };
}

export function loading_off() {
    return { type: actionTypes.LOADING_OFF };
}

export function closeError() {
    return { type: actionTypes.SET_ERROR, data: null };
}

// Show Login if user is unlogged
export function showLogin() {
    return dispatch => {
        dispatch(loading_off());
        dispatch(switchLoadInit());
    }
}

export function getUserInfo() {
    return async (dispatch) => {
        try {
            const data = await api.getUserInfo();

            dispatch({ type: actionTypes.GET_USER_INFO, data });

            // If has accounts set first page
            if(!!data.accounts.data.length)
                dispatch(setPage(0));
        }catch(err) {
            dispatch({ type: actionTypes.GET_NOTIFICATIONS, data: { error: JSON.stringify(err) }});
        }

        dispatch(loading_off());
        dispatch({ type: actionTypes.OFF_LOADING_INIT });
        dispatch({ type: actionTypes.HIDE_LOGIN });
    };
}

// pag: index of page in accounts field
export function setPage(pag) {
    return (dispatch, getStore) => {
        let { data } = getStore().user.accounts;

        dispatch({ type: actionTypes.SET_PAGE, data: data[pag] });

        dispatch(getNotifications());
        dispatch(getInbox());
        dispatch(getPosts());
        dispatch(getInsta());
    }
}

export function getNotifications() {
    return async (dispatch, getStore) => {
        const { actualPage } = getStore();

        dispatch({ type: actionTypes.GET_NOTIFICATIONS, data: { list: [], loading: true, error: null }});

        try {
            const { data } = await api.getNotifications(actualPage.id, actualPage.access_token);

            dispatch({ type: actionTypes.GET_NOTIFICATIONS, data: { list: data, loading: false, error: null }});
        }catch(err) {
            dispatch({ type: actionTypes.GET_NOTIFICATIONS, data: { error: JSON.stringify(err) }});
        }
    };
}

/**
 * 
 * POST Actions
 * 
*/

export function setPosts(data) {
    return (dispatch) => dispatch({ type: actionTypes.SET_POSTS, data });
}

export function sendPost(mensagem, url, picture = null, isInsta) {
    return async (dispatch, getState) => {
        let id, list;
        let { access_token } = getState().actualPage;
        if(!!isInsta) {
            id = getState().instagram.id;
            list = getState().instagram.list;
        }else {
            id = getState().actualPage.id;
            list = getState().posts.list;
        }

        dispatch(loading_on());

        try {
            if(!!url) {
                var res = await api.sendMedia(id, access_token, mensagem, url, isInsta);
            }else {
                var res = await api.sendPost(id, access_token, mensagem, isInsta);
            }

            let newPost = { 
                id: res.id, 
                comments: [], 
                created_time: Moment().format(), 
                message: mensagem, 
                picture, 
                likes: {
                    data: [],
                    summary: {total_count: 0, can_like: true, has_liked: false}
                } 
            };

            var newList = Object.keys(list).reduce((ac, k, i) => {
                if(i == 0)
                    ac[res.id] = newPost;

                ac[k] = list[k];

                return ac;
            }, {});

            if(isInsta) {
                dispatch({ type: actionTypes.GET_INSTA, data: { id, list: newList, loading: false, error: null }});
            }else {
                dispatch({ type: actionTypes.GET_POSTS, data: { list: newList, loading: false, error: null }});
            }
        }catch(err) {
            dispatch({ type: actionTypes.SET_ERROR, data: JSON.stringify(err) })
        }

        dispatch(loading_off());
    }
}

export function getPosts() {
    return async (dispatch, getState) => {
        const { actualPage } = getState();

        dispatch({ type: actionTypes.GET_POSTS, data: { list: {}, loading: true, error: null }});

        try {
            const { data } = await api.getPosts(actualPage.id, actualPage.access_token);

            var list = data.reduce((obj, p) => {
                obj[p.id] = { ...p, comments: null, insights: null, showComments: false, showInsights: false };

                return obj;
            }, {});

            dispatch({ type: actionTypes.GET_POSTS, data: { list, loading: false, error: null }});
        }catch(err) {
            dispatch({ type: actionTypes.GET_POSTS, data: { error: JSON.stringify(err) }});
        }  
    };
}

export function getComment(id, isInsta) {
    return async (dispatch, getStore) => {
        let { actualPage, posts, instagram } = getStore();

        var allPosts = !!isInsta ? instagram : posts;

        if(!allPosts.list[id].comments) {
            try {
                dispatch(loading_on());

                const { data } = await api.getComments(id, actualPage.access_token);

                let comments = data.reduce((obj, c) => {
                    obj[c.id] = c;

                    return obj;
                }, {});

                allPosts.list[id] = { ...allPosts.list[id], comments, showComments: true };

                if(isInsta) {
                    dispatch(setPostsInsta({ list: allPosts.list, loading: false, error: null }));
                }else {
                    dispatch(setPosts({ list: allPosts.list, loading: false, error: null }));
                }
            
                dispatch(loading_off());
            }catch(err) {
                dispatch({ type: actionTypes.SET_ERROR, data: JSON.stringify(err) });
            }
        }else {
            allPosts.list[id].showComments = !allPosts.list[id].showComments;

            dispatch(isInsta ? setPostsInsta({ list: allPosts.list, loading: false, error: null }) : setPosts({ list: allPosts.list, loading: false, error: null }));
        }
    }
}

export function getInsights(id) {
    return async (dispatch, getStore) => {
        let { actualPage, posts } = getStore();

        if(!posts.list[id].insights) {
            try {
                dispatch(loading_on());

                const { data } = await api.getInsights(id, actualPage.access_token);

                let insights = data.map(({ values }, i) => {
                    switch(i) {
                        case 0:
                            return ["Post organic", values[0].value];
                        case 1:
                            return ["Post Paid", values[0].value];
                        case 2:
                            return ["Negative feedback", values[0].value];
                        case 3:
                            return ["Engaged Users", values[0].value];
                    }
                });

                posts.list[id] = { ...posts.list[id], insights, showInsights: true };

                dispatch(setPosts({ list: posts.list, loading: false, error: null }));

                dispatch(loading_off());
            }catch(err) {
                dispatch({ type: actionTypes.SET_ERROR, data: JSON.stringify(err)});
            }
        }else {
            posts.list[id] = { ...posts.list[id], showInsights: !posts.list[id].showInsights };

            dispatch(setPosts({ list: posts.list, loading: false, error: null }));
        }
    }
}

export function getCommentsNest(postId, id) {
    return async (dispatch, getStore) => {
        let { actualPage, posts } = getStore();
        
        try {
            const { data } = await api.getComments(id, actualPage.access_token);

            posts.list[postId].comments[id].comments = data;
            posts.list[postId].comments[id].fetched = true;
            
            dispatch(setPosts({ list: posts.list, loading: false, error: null }));
        }catch(err) {
            dispatch({ type: actionTypes.SET_ERROR, data: JSON.stringify(err) })
        }
    }
}

export function deletePosts(id) {
    return async (dispatch, getState) => {
        let { actualPage, posts } = getState();

        dispatch(loading_on());

        try {
            const { data } = await api.deletePost(id, actualPage.access_token);

            delete posts.list[id];

            dispatch({ type: actionTypes.GET_POSTS, data: { list: posts.list }})
        }catch(err) {
            dispatch({ type: actionTypes.SET_ERROR, data: JSON.stringify(err) })
        }

        dispatch(loading_off());
    }
}

export function likePost(postId) {
    return async (dispatch, getState) => {
        var { posts, actualPage } = getState();
        var post = posts.list[postId];

        try {
            if(post.likes.summary.has_liked) {
                var res = await api.removeLikePost(postId, actualPage.access_token);
                posts.list[postId].likes.summary.has_liked = false;
                posts.list[postId].likes.summary.total_count--;
            }else {
                var res = await api.likePost(postId, actualPage.access_token);
                posts.list[postId].likes.summary.has_liked = true;
                posts.list[postId].likes.summary.total_count++;
            }

            if(res.success) {
                dispatch({ type: actionTypes.GET_POSTS, data: { list: posts.list, loading: false, error: null }});
            }
        }catch(err) {
            dispatch({ type: actionTypes.SET_ERROR, data: JSON.stringify(err) })
        }
    }
}

export function addCommentNext(id, postId, msg, sub, isInsta) {
    return async (dispatch, getStore) => {
        let { actualPage, posts, instagram } = getStore();

        let allPosts = isInsta ? instagram : posts;

        try {
            const data = await api.sendComment(id || postId, actualPage.access_token, msg);

            let comment = { created_time: Moment().format(), from: actualPage, id: data.id, message: msg };

            if(sub) {
                allPosts.list[postId].comments[id].comments = [comment, ...allPosts.list[postId].comments[id].comments];
            }else {
                allPosts.list[postId].comments[data.id] = comment;
            }

            dispatch(setPosts({ list: allPosts.list, loading: false, error: null }));
        }catch(err) {
            dispatch({ type: actionTypes.SET_ERROR, data: JSON.stringify(err) })
        }
    }
}

/**
 * 
 * Instagram Actions
 * 
*/

export function setPostsInsta(data){
    return { type: actionTypes.GET_INSTA, data };
}

export function getInsta() {
    return async (dispatch, getState) => {
        const { actualPage } = getState();
        try {
            let data = await api.pageInsta(actualPage.id);

            if(data) {
                let list = data.media.data.reduce((obj, p) => {
                    obj[p.id] = { ...p, showComments: false, showInsights: false, comments: null, insights: null };

                    return obj;
                }, {});

                dispatch({ type: actionTypes.GET_INSTA, data: { id: data.id, list } });
            }else {
                dispatch({ type: actionTypes.GET_INSTA, data: null })
            }
        }catch(err) {
            dispatch({ type: actionTypes.GET_INSTA, data: { error: JSON.stringify(err)}})
        }
    }
}

export function getInsightsInsta(id) {
    return async (dispatch, getStore) => {
        let { actualPage, instagram } = getStore();

        if(!instagram.list[id].insights) {
            try {
                dispatch(loading_on());

                const { data } = await api.getInsightsInsta(id, actualPage.access_token);

                var insights = data.map(({ values }, i) => {
                    switch(i) {
                        case 0:
                            return ["Impressions", values[0].value];
                        case 1:
                            return ["Reach", values[0].value];
                    }
                })

                instagram.list[id] = { ...instagram.list[id], showInsights: true, insights};

                dispatch(setPostsInsta({ list: instagram.list, loading: false, error: null }));
            
                dispatch(loading_off());
            }catch(err) {
                dispatch({ type: actionTypes.SET_ERROR, data: JSON.stringify(err) })
            }
        }else {
            instagram.list[id].showInsights = !instagram.list[id].showInsights;
            dispatch(setPostsInsta({ list: instagram.list, loading: false, error: null }));
        }
    }
}

/**
 * 
 * Inbox Function
 * 
*/

export function getInbox() {
    return async (dispatch, getState) => {
        const { actualPage } = getState();
        
        dispatch({ type: actionTypes.GET_INBOX, data: { list: [], loading: true, error: null }});
        
        try {
            const { data } = await api.getInbox(actualPage.id, actualPage.access_token);

            dispatch({ type: actionTypes.GET_INBOX, data: { list: data, loading: false, error: null }});

            data.forEach(async (inbx, i) => {
                const { list } = getState().inbox; 

                try {
                    let user = await api.getUserPic(inbx.participants.data[0].id);
                    inbx.picture = user.data.url;
                }catch(err) {
                    inbx.picture = "https://static.xx.fbcdn.net/rsrc.php/v3/yo/r/UlIqmHJn-SK.gif";
                }

                list[inbx.id] = inbx;
                delete list[i];

                dispatch({ type: actionTypes.GET_INBOX, data: { list, loading: false, error: null }});
            });
        }catch(err) {
            dispatch({ type: actionTypes.GET_INBOX, data: { error: JSON.stringify(err) }});
        }
    };
}

export function openInbox(data) {
    return { type: actionTypes.OPEN_INBOX, data };
}

export function closeInbox() {
    return { type: actionTypes.CLOSE_INBOX };
}

export function sendInbox(id, msg) {
    return async (dispatch, getStore) => {
        let { actualPage, inbox } = getStore();

        dispatch(loading_on());

        try {
            var data = await api.sendInbox(id, actualPage.access_token, msg);

            let newMessage = { created_time: Moment().format(), from: {actualPage}, id: data.id, message: msg };

            inbox.list[id].messages.data.unshift(newMessage);
            inbox.list[id].snippet = msg;
            inbox.infoInbox.messages.data.unshift(newMessage);
            inbox.infoInbox.snippet = msg;

            dispatch({ type: actionTypes.GET_INBOX, data: inbox});
        }catch(err) {
            dispatch({ type: actionTypes.SET_ERROR, data: JSON.stringify(err) })
        }

        dispatch(loading_off());
    }
}